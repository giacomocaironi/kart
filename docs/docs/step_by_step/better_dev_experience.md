---
title: A better dev experience
---

The website is now functional, but there are some steps we things we can implement

## Reorganize the templates

Currently we have only one template file.
However if our site grows we are going to need many more templates.
And most of them will have a lot of things in common.
To avoid repeating ourselves we can use some features of Jinja2.

The first feature is the ability to include other templates.
For example if our navigation bar is used by many templates, we could move it to a new template, and then include it in the other templates, like this

```html
...
  <body>

    {% include 'nav.html' %}

    <h1>{{page.title}}</h1>

    {{page.content|html}}

  </body>
</html>
```

Obviously ``nav.html`` should be the name of the file which you have created.

The second feature is template inheritance.
We could create a base template which contains all the boilerplate so that the child templates only concentrate on the important stuff.
For example we could have a ``base.html``

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {% block title %}
      <title></title>
    {% endblock title %}
  </head>
  <body>

    {% block content %}
    {% endblock content %}

  </body>
</html>
```

And then a child template which inherits from ``base.html``

```html
{% extends "base.html" %}

{% block content %}

  {% include 'nav.html' %}

  <h1>{{page.title}}</h1>

  {{page.content|html}}

{% endblock content %}
```

You can learn more about template inheritance [here](https://jinja.palletsprojects.com/en/3.0.x/templates/#template-inheritance).

Now that we have only one template what we have talked about is not really necessary, but this tips will save you a lot of time if you start to have a lot of templates.

## Hot reload

It would be very useful if Kart reload the webpage after every change to a markdown file or template, to speedup the development of our website.
Fortunately Kart does this for us!

to start using this feature we only have to modify the ``main.py`` and our base template file.

```python
...

k.miners = [
  miners.DefaultPageMiner(),
  miners.WatchDirectoryMiner(["templates"])
]

...
```

The default miners implement the logic to watch for file changes, but there are directories, like the template folder, that have no miner attached to them.
Thus we use a new miner, ``WatchDirectoryMiner``, which only activity is to watch for file changes in a list of directories.

Next we have to modify the base template file to include the reload script during development

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    {% block title %}
      <title></title>
    {% endblock title %}
  </head>
  <body>

    {% block content %}
    {% endblock content %}

    {% if config.serving %}
        <script src="{{ url("/hot_reload") }}"></script>
    {% endif %}

  </body>
</html>
```

Done! Now every time we modify a page the webserver automatically reloads the page for us!
