from importlib import metadata

from kart import Kart, mappers, miners, renderers
from kart.ext import documentation
from kart.utils import date_to_string

kart = Kart()

kart.miners = [
    miners.WatchDirectoryMiner(["root", "static", "templates"]),
    miners.DefaultDataMiner(),
    documentation.DefaultDocumentationMiner(),
]

kart.content_modifiers = []

kart.mappers = [
    documentation.DefaultDocumentationMapper(template="default.html"),
    mappers.DefaultSitemapMapper(),
    mappers.DefaultStaticFilesMapper(),
    mappers.DefaultRootDirMapper(),
]

kart.map_modifiers = []

kart.renderers = [
    renderers.DefaultSiteRenderer(
        filters={
            "html": documentation.markdown_to_html,
            "toc": documentation.markdown_to_toc,
            "date_to_string": date_to_string,
        }
    ),
    renderers.DefaultSitemapRenderer(),
    renderers.DefaultStaticFilesRenderer(),
    renderers.DefaultRootDirRenderer(),
]

kart.config["name"] = "{{cookiecutter.project_name}}"
kart.config["copyright"] = "{{cookiecutter.copyright}}"
kart.config["icon"] = "https://github.com/favicon.ico"
kart.config["version"] = metadata.version(kart.config["name"])
kart.config["repo_url"] = "{{cookiecutter.repository_url}}"
kart.config["repo_name"] = "{{cookiecutter.repository_name}}"
kart.config["site_url"] = "{{cookiecutter.site_url}}"
kart.config["code_highlighting"] = {"style": "material"}

if __name__ == "__main__":
    kart.run()
